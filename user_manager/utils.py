from django.db import connection
from django.http import Http404


def dict_fetch_all(cursor):
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row))
        for row in cursor.fetchall()
    ]


def get_user_list(page, row_count, search):
    paginator = Paginate(page, row_count, search)
    cursor = connection.cursor()
    cursor.execute("SELECT id, name, email, status FROM user where name like %s limit %s, %s;",
                   ['%{}%'.format(search), paginator.offset, paginator.row_count])
    return paginator, dict_fetch_all(cursor)


def get_course_list():
    cursor = connection.cursor()
    cursor.execute("SELECT id, name, code FROM courses;")
    return dict_fetch_all(cursor)


def new_user(data):
    cursor = connection.cursor()
    name = data['name']
    email = data['email']
    mobile = data['mobile'] or None
    phone = data['phone'] or None
    status = data['status']
    cursor.execute("call create_user(%s, %s, %s, %s, %s);", [name, email, mobile, phone, status])
    cursor.fetchone()


def change_user(user_id, data):
    cursor = connection.cursor()
    name = data['name']
    email = data['email']
    mobile = data['mobile'] or None
    phone = data['phone'] or None
    status = data['status']
    courses = data['courses']
    cursor.execute("call edit_user(%s, %s, %s, %s, %s, %s);", [user_id, name, email, mobile, phone, status])
    cursor.execute("call delete_user__courses(%s);", [user_id])
    for course in courses:
        cursor.execute("call insert_user__courses(%s, %s);", [user_id, course])
    cursor.fetchall()


def remove_user(user_id):
    cursor = connection.cursor()
    cursor.execute("call delete_user(%s);", [user_id])
    cursor.fetchone()


def get_all_curses():
    cursor = connection.cursor()
    cursor.execute("SELECT courses.id, courses.name FROM courses", [])
    return cursor.fetchall()


def get_user_data(user_id):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM user WHERE user.id=%s", [user_id])
    user_data = dict_fetch_all(cursor)[0]
    cursor.execute("SELECT c.id FROM users__courses u_c join courses c on u_c.courses_id=c.id"
                   " WHERE u_c.user_id=%s;", [user_id])
    courses = cursor.fetchall()
    user_data['courses'] = [course[0] for course in courses]
    return user_data


class Paginate(object):
    def __init__(self, current_page, row_count, search):
        cursor = connection.cursor()
        cursor.execute("select count(*) from user  where name like %s;", ['%{}%'.format(search)])
        try:
            self.current_page = int(current_page)
            self.row_count = int(row_count)
            count_of_rows = int(cursor.fetchone()[0])
            max_page = count_of_rows / self.row_count
            if count_of_rows % self.row_count > 0:
                max_page += 1
            self.max_page = max_page or 1
            self.offset = (self.current_page - 1) * self.row_count
        except:
            raise Http404
        if self.current_page < 1 or self.row_count < 1 or self.max_page < self.current_page:
            raise Http404

    def has_previous(self):
        return self.current_page > 1

    def previous_page_number(self):
        return self.current_page - 1

    def next_page_number(self):
        return str(self.current_page + 1)

    def has_next(self):
        return self.current_page < self.max_page

    def page_range(self):
        min_page = 1
        max_page = self.max_page
        try_move_left = True
        try_move_right = True
        if self.current_page > 1:
            min_page = self.current_page - 1
            try_move_right = False
        if self.current_page <= self.max_page - 3:
            max_page = self.current_page + 3
            try_move_left = False

        if try_move_right:
            if self.current_page + 4 <= self.max_page:
                max_page = self.current_page + 4
            else:
                max_page = self.max_page
        if try_move_left:
            if self.max_page > 4:
                min_page = self.max_page - 4
            else:
                min_page = 1
        return range(min_page, max_page + 1)