from django import forms

from user_manager.utils import get_all_curses


class CreateUserForm(forms.Form):
    STATUS = (
        ('', 'Inactive'),
        (1, 'Active')
    )
    name = forms.CharField(max_length=244)
    email = forms.EmailField()
    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$', required=False, error_message=(
        "Format phone number : '+999999999'."" Up to 15 digits allowed."))
    mobile = forms.RegexField(regex=r'^\+?1?\d{9,15}$', required=False, error_message=(
        "Format phone number : '+999999999'."" Up to 15 digits allowed."), label='Mobile phone')
    status = forms.ChoiceField(required=False, choices=STATUS)


class EditUserForm(forms.Form):
    STATUS = (
        ('', 'Inactive'),
        (1, 'Active')
    )

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        self.fields['courses'] = forms.MultipleChoiceField(required=False, choices=get_all_curses())
        self.fields['name'].widget.attrs['readonly'] = True
    name = forms.CharField(max_length=244)
    email = forms.EmailField()
    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$', required=False, error_message=(
        "Format phone number : '+999999999'."" Up to 15 digits allowed."))
    mobile = forms.RegexField(regex=r'^\+?1?\d{9,15}$', required=False, error_message=(
        "Format phone number : '+999999999'."" Up to 15 digits allowed."), label='Mobile phone')
    status = forms.ChoiceField(required=False, choices=STATUS)
    courses = forms.MultipleChoiceField()
