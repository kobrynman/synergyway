from django.conf.urls import url

from user_manager import views
from user_manager.views import delete_user, courses

urlpatterns = (
    url(r'^$', views.user_list, name='user_list'),
    url(r'^create_user/$', views.create_user, name='create_user'),
    url(r'^edit_user/(?P<pk>\d+)/$', views.edit_user, name='edit_user'),
    url(r'^delete_user/(?P<pk>\d+)/$', delete_user, name='delete_user'),
    url(r'^courses/$', courses, name='courses'),
)