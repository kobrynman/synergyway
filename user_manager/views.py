from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.urls import reverse
from django.views.decorators.cache import cache_page
from django.views.decorators.http import require_GET

from user_manager.forms import CreateUserForm, EditUserForm
from user_manager.utils import get_user_list, new_user, get_user_data, change_user, remove_user, get_course_list


@require_GET
def user_list(request):
    page = request.GET.get('page', 1)
    search = request.GET.get('search', '')
    if 'row_count' in request.COOKIES:
        row_count = request.COOKIES.get('row_count')
    else:
        row_count = 10
    paginator, users = get_user_list(page, row_count, search)
    context = {'users': users, 'paginator': paginator, 'search': search, 'row_count': row_count}
    return render_to_response('user_manager/user_list.html', context)


def create_user(request):
    if request.method == 'GET':
        form = CreateUserForm()
        context = {'form': form}
        return render_to_response('user_manager/create_user.html', context)
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest(form.errors.as_json())
        new_user(form.cleaned_data)
        return HttpResponse('')


def edit_user(request, pk):
    if request.method == 'GET':
        user_data = get_user_data(pk)
        form = EditUserForm(user_data)
        context = {'form': form}
        return render_to_response('user_manager/edit_user.html', context)
    if request.method == 'POST':
        form = EditUserForm(request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest(form.errors.as_json())
        change_user(pk, form.cleaned_data)
        return HttpResponse('')


def delete_user(request, pk):
    remove_user(pk)
    return HttpResponseRedirect(reverse('user_list'))


@cache_page(60 * 15)
def courses(request):
    courses_list = get_course_list()
    context = {'courses': courses_list}
    return render_to_response('user_manager/courses.html', context)
