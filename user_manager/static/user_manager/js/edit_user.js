$( document ).ready(function() {
    var select = $('<select>', { 'class': 'form-control round'}).append($('<tbody>'));
    $('#id_courses option').each(function (i, sel) {
        var option = $('<option>',{ 'value': $(sel).val(), 'text': $(sel).text()});
        select.append(option);
        if($(sel).is(':selected')){
            option.addClass('hidden')
        }
     });
    $('#selector_courses div:first').append(select);
    $("#selector_courses select").val($("#selector_courses select option:not([class='hidden']):first").val());

    $("#id_courses :selected").each(function (i, sel) {
        add_course($(sel).val(), $(sel).text());
     });

    $("#add_course").click(function(){
        var course_id = $("#selector_courses select").val();
        var course_name= $("#selector_courses select option:selected").text()
        $("#selector_courses select option[value=" + course_id +"]").addClass('hidden');
        if($("#selector_courses select option:not([class='hidden'])").length) {
            $("#selector_courses select").val($("#selector_courses select option:not([class='hidden']):first").val());
        } else {
            $("#selector_courses").addClass('hidden');
        }
        $("#id_courses option[value=" + course_id +"]").attr("selected", "selected");
        add_course(course_id, course_name)
    });

    $("#edit_user").click(function(){
        var data = $(this).closest('form').serialize();
        $.ajax({
            type: "POST",
            url: window.location.pathname,
            data: data,
            success: function (data) {
                $('#response').html('Changes saved successfully');
                $('#response').removeClass( "hidden" );
                setTimeout(function() {
                    window.location.replace("/");
                }, 3000);
            },
            error: function (data) {
                var error;
                var form_error = jQuery.parseJSON(data.responseText);
                for (var property in form_error) {
                    if (form_error.hasOwnProperty(property)) {
                        error = $("<span></span>", {'text': form_error[property][0].message, 'class': 'error'});
                        var field = $("#form_edit_user [name='" + property +"']").closest('div').next();
                        field.append(error);
                        field.nextAll(".error").remove();
                        field.after(error);
                    }
                }
            }
        });
    })
});

function add_course(val, text) {
    var td2 = $('<td>').append($('<a>', {'id': "course_" + val, 'class': 'right-text', 'onclick':
    'delete_course(' + val + ')'}).append($('<i>', { 'class': 'glyphicon glyphicon-remove-circle'})));
    var td1 = $('<td>', {'text': text});
    $("table tbody").append($('<tr>',{ 'class': 'text-left'}).append([td1, td2]));
}

function delete_course(id) {
    $("#course_" + id).closest('tr').remove();
    $("#selector_courses option[value=" + id +"]").removeClass('hidden');
    $("#selector_courses").removeClass('hidden');
    $("#id_courses option[value=" + id +"]").removeAttr("selected");
}
