$( document ).ready(function() {
    $("#create_user").click(function(){
        var data = $(this).closest('form').serialize();
        $.ajax({
            type: "POST",
            url: "/create_user/",
            data: data,
            success: function (data) {
                $('#response').html('User created successfully');
                $('#response').removeClass( "hidden" );
                setTimeout(function() {
                    window.location.replace("/");
                }, 3000);
            },
            error: function (data) {
                var error;
                var form_error = jQuery.parseJSON(data.responseText);
                for (var property in form_error) {
                    if (form_error.hasOwnProperty(property)) {
                        error = $("<span></span>", {'text': form_error[property][0].message, 'class': 'error'});
                        var field = $("#form_create_user [name='" + property +"']").closest('div').next();
                        field.append(error);
                        field.nextAll(".error").remove();
                        field.after(error);
                    }
                }
            }
        });
    })
});
