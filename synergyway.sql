DROP Database IF EXISTS Users;
CREATE Database Users;
GRANT SELECT ON Users.* TO 'synergyway_user'@'localhost' identified BY 'synergyway_pass';
USE Users;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `mobile` varchar(13) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL,
  `code` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `courses` VALUES
(1,'Python-Base', 'P012345'),
(2,'Python-Database', 'P234567'),
(3,'HTML', 'H345678'),
(4,'Java-Base', 'J456789'),
(5,'JavaScript-Base', 'JS543210');


CREATE TABLE `users__courses` (
  `user_id` int(11) NOT NULL,
  `courses_id` int(11) NOT NULL,
  PRIMARY KEY (user_id, courses_id),
  FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
  FOREIGN KEY (courses_id) REFERENCES courses (id)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP procedure IF EXISTS `create_user`;
DELIMITER $$
CREATE PROCEDURE `create_user` (
  n varchar(255),
  e varchar(254),
  p varchar(13),
  m varchar(13),
  s tinyint(1)
)
BEGIN
	insert into user  (name, email, phone, mobile, status) value (n, e, p, m, s);
END$$
DELIMITER ;


DROP procedure IF EXISTS `edit_user`;
DELIMITER $$
CREATE PROCEDURE `edit_user` (
  user_id int(11),
  n varchar(255),
  e varchar(254),
  p varchar(13),
  m varchar(13),
  s tinyint(1)
)
BEGIN
  UPDATE user
      SET name=n,
          email=e,
          phone=p,
          mobile=m,
          status=s
      WHERE id = user_id;
END$$
DELIMITER ;


DROP procedure IF EXISTS `delete_user__courses`;
DELIMITER $$
CREATE PROCEDURE `delete_user__courses` (
  user_id int(11)
)
BEGIN
  delete from users__courses
  where users__courses.user_id = user_id;
END$$
DELIMITER ;


DROP procedure IF EXISTS `insert_user__courses`;
DELIMITER $$
CREATE PROCEDURE `insert_user__courses` (
  user_id int(11),
  courses_id int(11)
)
BEGIN
  insert into users__courses  (user_id, courses_id) value (user_id, courses_id);
END$$
DELIMITER ;


DROP procedure IF EXISTS `delete_user`;
DELIMITER $$
CREATE PROCEDURE `delete_user` (
  user_id int(11)
)
BEGIN
  delete from user
  where user.id = user_id;
END$$
DELIMITER ;
